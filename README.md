# Recipe App API Proxy

NGINIX proxy app for our receipe app API

## Usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen on (default: '8080')
* 'APP_HOST' - Hostname of the app to forward request to (default: 'app')
* 'APP_PORT' -Port of the app to forward request to (defaut: '9000')